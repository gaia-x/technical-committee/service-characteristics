# Markdown Templates

These template files are used to transform LinkML entities into Markdown files using the `gen-doc` command [documented
here](https://linkml.io/linkml/generators/markdown.html).

We based ourselves on the [default tempalte files](https://github.com/linkml/linkml/tree/main/linkml/generators/docgen)
and we tweaked them to capitalize index names for example.
