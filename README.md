# GAIA-X Service Characteristics

This repository contains schema and documentation for developing compliant Gaia-X Credentials (formerly called Gaia-X Self-Descriptions / SDs).

Credentials ensure the fulfillment of the policies and rules agreed by the participants that decide to build a data ecosystem under the governance defined according to the Gaia-X Association.

To build and validate Credentials, the schema is made available through the [Gaia-X Compliance Services](https://gitlab.com/gaia-x/lab/compliance).

## GitLab Structure

Folders:
- `linkml`: Gaia-X Ontology described by linkml. Ontology contains of a hierarchy of classes, called taxonomy and a set of attributes for each class. Attributes are encoded in YAML files. 
- `instances`: Sample credentials in JSON-LD.
- `toolchain`: Scripts and unit tests for our CI/CD pipeline.
- `deprecated`: Files pending to convert to LinkML (eventually will be removed).

## Documentation

Documentation for the Gaia-X Ontology can be found at https://w3id.org/gaia-x/.

## How to Use the Ontology ?

The Gaia-X ontology has been built with extensibility and mass adoption in mind, so we aim to offer multiple ways of
using and extending the ontology.

### Using Through w3id.org

To broadcast the Gaia-X ontology schemas, we rely on
the [perma-id w3id.org project](https://github.com/perma-id/w3id.org)
which allows to publish our schemas via a single HTTPS endpoint.

```
https://w3id.org/gaia-x/
```

If you resolve the above URL, it will guide you to the Gaia-X ontology documentation; but it has many more features when
resolved with a few additional elements which are described in the following chapters.

All the following chapters use a URL with the `https://w3id.org/gaia-x/{version}#` pattern where `{version}` corresponds
to the version you wish to reference. Then the wanted content type can be specified through the HTTP `Accept` request
header.

> ⚠️ Please note that currently, only `development` is available and it references our latest **unstable** development
> version.

#### SHACL Shapes

The Gaia-X SHACL shapes can be retrieved with the following query:

```http
GET /gaia-x/development# HTTP/1.1
Host: w3id.org
Accept: text/turtle
```

If you were to extend the Gaia-X SHACL shapes to add `mygx:MyLegalPerson`, a child of `gx:LegalPerson`, you could use
the following snippet:

```turtle

@prefix sh:   <http://www.w3.org/ns/shacl#> .
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xsd:  <http://www.w3.org/2001/XMLSchema#> .
@prefix gx:   <https://w3id.org/gaia-x/development#> .
@prefix mygx: <https://my-gaia-x.eu#> .

gx:MyLegalPersonShape
  a                    gx:LegalPerson ;
  sh:closed            true ;
  sh:description       "A custom definition of the Gaia-X LegalPerson" ;
  sh:ignoredProperties ( rdf:type ) ;
  sh:property          [ sh:datatype xsd:string ;
                         sh:minCount 1 ;
                         sh:nodeKind sh:Literal ;
                         sh:order    0 ;
                         sh:path     mygx:myAdditionalAttribute ] ;
  sh:targetClass       mygx:MyLegalPerson .
```

> ✋ We suggest you extends the Gaia-X ontology directly with LinkML as described in the
> [Extending Through LinkML chapter](#extending-through-linkml).

#### JSON-LD Context

The Gaia-X JSON-LD context can be retrieved with the following query:

```http
GET /gaia-x/development# HTTP/1.1
Host: w3id.org
Accept: application/ld+json
```

Basing ourselves on the example of the [SHACL Shapes chapter](#shacl-shapes), a `mygx:MyLegalPerson` object would look
like the following example:

```json
{
  "@context": {
    "gx": "https://w3id.org/gaia-x/development#",
    "mygx": "https://my-gaia-x.eu#"
  },
  "@id": "https://my-gaia-x.eu/myLegalPerson",
  "@type": "mygx:MyLegalPerson",
  "mygx:myAdditionalAttribute": "Additional Attribute"
}
```

> ✋ We suggest you extends the Gaia-X ontology directly with LinkML as described in the
> [Extending Through LinkML chapter](#extending-through-linkml).

#### OWL Ontology

The Gaia-X OWL ontology can be retrieved with the following query:

```http
GET /gaia-x/development# HTTP/1.1
Host: w3id.org
Accept: application/rdf+xml
```

### Extending Through LinkML

The best way to extend the Gaia-X ontology is to use
[the powers of LinkML's `import` keyword](https://linkml.io/linkml/schemas/imports.html).

In order to produce the same `mygx:LegalPerson` entity as the [SHACL Shapes chapter](#shacl-shapes), the following
LinkML snippet can be used:

```yaml
id: https://my-gaia-x.eu#my-ontology
name: my-ontology

default_prefix: mygx
prefixes:
  gaia-x: https://w3id.org/gaia-x/development/linkml/
  mygx: https://my-gaia-x.eu#

imports:
  - gaia-x:types

classes:
  MyLegalPerson:
    title: "My Legal Person"
    is_a: LegalPerson
    description: A custom definition of the Gaia-X LegalPerson
```

### JavaScript/TypeScript

An NPM package is made available by this repository to allow developers to use Gaia-X entities, shapes and contexts in
their JavaScript or TypeScript project.

You can find this package here 👉 https://www.npmjs.com/package/@gaia-x/ontology.

All you need to do is install the Gaia-X ontology NPM package with your favorite dependency management system.

```bash
# NPM
npm install --save @gaia-x/ontology

# Yarn
yarn add @gaia-x/ontology
```

#### Versions

As NPM package versioning is done with a `<major>.<minor>.<patch>` pattern, ontology versions cannot be used.
The below table explains version correspondence.

| Ontology version | NPM package version |
|------------------|---------------------|
| 24.06            | 1.x.x               |


## How to collaborate

These are the steps you should take if you want to collaborate in the development or maintenance of the working streams that are carried out in the group.

- Check the list of current open issues or create a new one if necessary. You can do it [in the issues](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/) section.
- Describe the issue and add the appropriate label(s) to it. Be sure of tagging the issue status, especially use `status:in_progress`.
- Create a new branch to hold your changes and updates. Name of branch should start with the number of the issue, e.g. *195-governance-process* for issue [#195](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/issues/195).
- Write a YAML file(s) in LinkML format to describe a Gaia-X entity and address your issue ("entity" refers to any good or object of the Gaia-X Ecosystem). Learn how to do it in the [linkml](./linkml) section.
- Generate, from this LinkML schema, artifacts expressed in terms of commonly used web standards, such a JSON, OWL and SHACL.

At this point, you can get these artifacts in two main ways.

- Create a new merge request to merge your branch to target branch `develop`. This request (also any time you push new changes to your branch) will throw the CI/CD process, that will - among other tasks - generate a zip with these JSON, OWL and SHACL files.
- Build these files locally using the [LinkML generator tool](https://linkml.io/linkml/generators/index.html). 

Once you got the shapes:

- To check their validity, you need to build and run unit tests. Visit the [Unit test](./toolchain/test-shacl) section to get more details.
- To validate JSON instances, take a look at the [Toolchain](./toolchain) section.

When you have all the work done:

- Join our Weekly Meeting to present your request for change to the Sub-Working Group's members and discuss it with them. General information about the group and the links to join can be found at the [Member's platform](https://membersplatform.gaia-x.eu/dashboard?locale=en#/agenda/49). 
- Answer and resolve threads in your Merge Request. Note: Thread may be re-opened by other members.

**Guidelines for merge into develop**:

+ No unresolved thread, no merge conflicts either.
+ Successfully run CI/CD pipeline (this process also runs automatically every time you push new changes).
+ Presentation of changes in weekly Service Characteristics meeting. The request will require the approval from two members of the group, besides the one from CTO team representative.
+ Only merge requests with label `status:ready_for_approval` will be approved and merged.

**PLEASE NOTE: The author, this means you, is responsible for propagating relevant changes back to other Gaia-X deliverables. This will not happen automatically or by any other member of the community.**

## CI/CD

These are the stages that will be executed during the continuous integration process:   

- build: build Self-Description artifacts.
- test: test correctness of Self-Descriptions artefacts.
- validate: validate Self-Description samples.
- package: build and publish generated artifacts in package registry.
- pages: publish a Gitlab Pages website
- release: automatically create a new release from a tag.

## Ontology Version Management

Gaia-X ontology versions need to be easy to distinguish and reference. To do so, the context name contains the referenced
version in its URI.

```
https://w3id.org/gaia-x/{version}#
```

For example, this is the URI of the 24.04 Gaia-X ontology namespace.

```
https://w3id.org/gaia-x/2404#
```

Please notice that the `.` in the version number has been removed for convenience and technical reasons in the URI.

## Releasing

For security reasons, only users with a minimum of `Maintainer` access level are allowed to create a new release of this project.

Releases are automatically created and published once a tag is created, to do so the following command can be used.

```bash
git checkout main
git tag -a v<YOUR_VERSION>
```

Tags must always be created from the `main` branch as it houses production ready code. `YOUR_VERSION` must be replaced 
with the version you are releasing (ie. `22.10` for October 2022's specification version).

## Organization

- All communication is done via Mailing List. Please use the [Member's platform](https://membersplatform.gaia-x.eu/users/sign_in?locale=en) to join our group.
- We meet weekly on Friday 08:00-09:00 (Central European (Summer) Time).
- Sources:
  - [GitLab](https://gitlab.com/gaia-x/technical-committee/service-characteristics/): source code.
  - [Gaia-X Members Platform](https://membersplatform.gaia-x.eu/): meeting minutes and binary files.
- Lead: Christoph Lange-Bever, Vice-Lead: Anja Strunk.

