id: https://w3id.org/gaia-x/development#data-protection-policy
name: data-protection-policy
prefixes:
  gx: https://w3id.org/gaia-x/development#
  linkml: https://w3id.org/linkml/
imports:
  - linkml:types
  - address
  - quantity
  - resource
  - slots
default_prefix: gx
default_range: string

classes:

  DataProtectionPolicy:
    description: A container class for various data protection features, such as backup
      or replication
    abstract: true
    slots:
      - protectionFrequency
      - protectionRetention
      - protectionMethod

  BackupPolicy:
    description: Describe data protection features based on Backup for storage services
    is_a: DataProtectionPolicy
    slots:
      - backupLocation
      - backupReplication

  SnapshotPolicy:
    description: Describe data protection features based on Snapshot for storage services
    is_a: DataProtectionPolicy
    slots:
      - snapshotReplication

  ReplicationPolicy:
    description: Describe data protection features based on Replication for storage
      services
    is_a: DataProtectionPolicy
    slots:
      - synchronousReplication
      - consistencyType
      - replicaNumber
      - geoReplication

enums:

  ProtectionFrequency:
    permissible_values:
      Hourly:
        description: For describing protection policies triggered once an hour
      Daily:
        description: For describing protection policies triggered once a day
      Weekly:
        description: For describing protection policies triggered once a week
      Monthly:
        description: For describing protection policies triggered once a month
      Continuously:
        description: For describing protection policies triggered continuously, ie
          each time data are modified
      OnDemand:
        description: For describing unplanned protection policies triggered on demand

  ProtectionMethod:
    permissible_values:
      Full-Copy:
        description: For describing protection through full copy of protected data
      Incremental:
        description: For describing protection through differential copy of protected
          data, ie only modifications are copied

  GeoReplicationScope:
    permissible_values:
      Cross-Region:
        description: Whenever replication is span across multiple regions
      Cross-DC:
        description: Whenever replication is span across multiple datacenters within
          a region
      Cross-AZ:
        description: Whenever replication is span across multiple availability zones

  ConsistencyType:
    permissible_values:
      Strong:
        description: Strong consistency model, cf https://en.wikipedia.org/wiki/Consistency_model#Strong_consistency_models.
      Session:
        description: Eventual consistency model, cf https://en.wikipedia.org/wiki/Consistency_model#Session_guarantees.
      Eventual:
        description: Eventual consistency model, cf https://en.wikipedia.org/wiki/Eventual_consistency.
      Bounded-Staleness:
        description: Consistency model where the lag between replicas is bounded,
          either by a number of updates or a time limit.
      Other:
        description: Unspecified consistency model.
