id: https://w3id.org/gaia-x/development#data-exchange-wg
name: data-exchange-wg
prefixes:
  linkml: https://w3id.org/linkml/
  gx: https://w3id.org/gaia-x/development#
  dcterms: http://purl.org/dc/terms/
  dcat: http://www.w3.org/ns/dcat#
imports:
  - linkml:types
  - gaia-x-entity
  - service-offering
  - slots
default_range: string
default_curi_maps:
  - semweb_context

classes:

  DataProduct:
    is_a: ServiceOffering
    class_uri: gx:DataProduct
    slots:
      - providedBy
      - termsAndConditions
      - licenses
      - title
      - description
      - issued
      - obsoleteDateTime
      - hasPolicy
      - dataLicensors
      - dataUsageAgreements
      - aggregationOf
      - identifier
      - contactPoint
      - conformsTo
    slot_usage:
      providedBy:
        required: true
        range: DataProducer
      termsAndConditions:
        required: true
      licenses:
        required: true
      title:
        description: Title of the Data Product.
        required: true
      aggregationOf:
        required: true
      identifier:
        required: true
      obsoleteDateTime:
        description: Date time in ISO 8601 format after which Data Product is obsolete.

  SignatureCheckType:
    is_a: GaiaXEntity
    class_uri: gx:SignatureCheckType
    slots:
      - participantRole
      - mandatory
      - legalValidity
    slot_usage:
      participantRole:
        required: true
      mandatory:
        required: true
      legalValidity:
        required: true

  DataUsage:
    is_a: GaiaXEntity
    class_uri: gx:DataUsage
    slots:
      - loggingService

  DataProductUsageContract:
    is_a: GaiaXEntity
    class_uri: gx:DataProductUsageContract
    slots:
      - providedBy
      - consumedBy
      - dataProduct
      - signers
      - termOfUsage
      - notarizedIn
      - dataUsage
    slot_usage:
      providedBy:
        required: true
        description: A resolvable link to the Data Provider Declaration.
        range: DataProvider
      consumedBy:
        required: true
        range: DataConsumer
      signers:
        required: true
      termOfUsage:
        required: true
      dataUsage:
        required: true
        range: DataUsage

  DataUsageAgreement:
    class_uri: gx:DataUsageAgreement
    slots:
      - producedBy
      - providedBy
      - licensedBy
      - dataUsageAgreementTrustAnchor
      - dataProduct
      - signers
    slot_usage:
      producedBy:
        required: true
        description: A resolvable link to the Data Producer.
        range: DataProducer
      providedBy:
        required: true
        description: A resolvable link to the Data Provider.
        range: DataProvider
      dataUsageAgreementTrustAnchor:
        required: true
      signers:
        required: true

  DataSet:
    is_a: GaiaXEntity
    class_uri: gx:DataSet
    slots:
      - title
      - distributions
      - identifier
      - issued
      - expirationDateTime
      - licenses
      - dataLicensors
      - dataUsageAgreements
      - exposedThrough
    slot_usage:
      title:
        description: Title of the Dataset.
        required: true
        slot_uri: dcterms:title
      distributions:
        required: true
        range: Distribution
      identifier:
        required: true

  Distribution:
    is_a: GaiaXEntity
    class_uri: dcat:Distribution
    slots:
      - title
      - format
      - compressFormat
      - packageFormat
      - byteSize
      - locations
      - hash
      - hashAlgorithm
      - issued
      - expirationDateTime
      - language
      - licenses
      - dataLicensors
      - dataUsageAgreements
    slot_usage:
      title:
        description: Title of the Distribution.
        required: true
        slot_uri: dcterms:title
      format:
        required: true
      hash:
        description: To uniquely identify the data contained in the dataset distribution.

  DataProvider:
    is_a: LegalPerson
    class_uri: gx:DataProvider
    description: is equivalent to Gaia-X Provider

  DataLicensor:
    is_a: LegalPerson
    class_uri: gx:DataLicensor
    description: is equivalent to Gaia-X Licensor

  DataProducer:
    is_a: LegalPerson
    class_uri: gx:DataProducer
    description: is equivalent to Gaia-X ResourceOwner

  DataConsumer:
    is_a: LegalPerson
    class_uri: gx:DataConsumer
    description: is equivalent to Gaia-X Consumer

  Federator:
    is_a: LegalPerson
    class_uri: gx:Federator
    description: is equivalent to Gaia-X Federator
