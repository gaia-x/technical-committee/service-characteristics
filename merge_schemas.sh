#!/bin/bash

####################################################################################################################
####################################################################################################################
###### Issue:             https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/105  ######
###### Author:            smartSense Consulting Solutions pvt. ltd.                                           ######
###### Website:           https://www.smartsensesolutions.com/                                                ######
###### LinkedIn Profile:  https://www.linkedin.com/company/smartsense-solutions/mycompany/                    ###### 
###### Developer:         Pinank Lakhani                                                                      ######
###### LinkedIn Profile:  https://www.linkedin.com/in/pinanklakhani/                                          ###### 
####################################################################################################################
####################################################################################################################

set -ex

# Determine which schema language to process based on the argument
if [[ "$1" == "shacl" ]]; then
  target="shapes"
elif [[ "$1" == "owl" ]]; then
  target="ontology"
else
  echo "No schema language argument provided. Please specify 'shacl' or 'owl'."
  exit 1
fi

generatedOutput="$target.linkml-generated.$1.ttl"
# Glob pattern to list all files in the desired language
schema_files=(linkml/*.$1.ttl $generatedOutput)
finalOutput="$target.$1.ttl"

if [[ "$1" == "shacl" ]]; then
  gen-shacl linkml/gaia-x.yaml --no-mergeimports --closed --suffix Shape > $generatedOutput
else
  gen-owl --no-use-native-uris --assert-equivalent-classes linkml/gaia-x.yaml > $generatedOutput
fi

# Check if there are any schema files to merge
if [[ ${#schema_files[@]} -eq 0 ]]; then
  echo "No additional $1 files found. Using the generated $1 file directly."
  mv $generatedOutput $finalOutput
else
  # schema files to merge into a single output file using rdfpipe
  rdfpipe --input-format=turtle --output-format=turtle "${schema_files[@]}" > $finalOutput
  echo "$finalOutput $1 schema file generated"
  rm $generatedOutput
fi