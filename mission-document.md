# Group Information

* Group Name: Service Characteristics
* Brief Description: Define the service characteristics by type of service

# Group roles and participants

# Objective / Scope (and Out of Scope)

## Objectives (min. 500 characters)

- providing shapes that enable the technical validation of the serialization of Gaia-X Credentials as graphs.
- define a taxonomy of terms (classes, attributes, datatypes, controlled vocabulary entries), refining the Conceptual Model of the Architecture Document and the Trust Framework, and implement it as an ontology. WG SvCh is the owner of this taxonomy. Any changes to this taxonomy must be in sync with the WG, technically by creating MRs in the SvCh GitLab.
  - for attributes related to law or trust, the decision on whether the cardinality of an attribute is 0 (optional) or 1 (mandatory) is with WG Compliance
- align Gaia-X terms (classes, attributes, controlled vocabulary entries) with terms defined by previously existing, external standards
- identify 3rd party trusted data sources for validating values of attributes against

## Out of Scope (min. 500 characters)

- Gaia-X Credential lifecycle
- query mechanisms and filtering
- encryption schema
- for now: refinements into specific directions (e.g., Big Data, AI, wallets) for which we lack expertise in the WG (but with the right human resources, this might end up "in scope")

# Deliverables according to Release plan

* Machine-executable credential schema (SHACL shapes and OWL ontology, i.e., "software") ready for publication in the Gaia-X Registry
* Human-readable documentation of the taxonomy of terms for the Gaia-X Credentials, including class diagrams and tables of attributes per class (document) ready for publication at docs.gaia-x.eu
* Contributions to the related conceptual parts of Architecture Document, Trust Framework, and ICAM (section "Credential Format")

TODO fill the following later once their meaning is clear:
* Type of Document
* Type of Delivery (Doc / SW)
* Document classification

# Major Known Risks (Please identify obstacles that may cause the group to fail.)

* Lack of consensus with other WGs that contribute to the Gaia-X Trust Framework, Architecture, Credentials and concepts for Data Exchange, Service Composition, etc.
* Lack of connection with the market's requirements (e.g., provider's and user's perspectives)
* Frictions in alignment with other relevant infrastructure initiatives (e.g., Simpl, IDSA, Data Spaces Support Centre)
* Technical complexity of engineering and maintaining the pipeline (CI/CD) that produces our deliverables

# Skills required for the Mission fulfilment (Please mention the defined skills per role and its set criteria)

* Lead, Co-Lead: no special skills required in addition to those mentioned below
* Contributing Participant:
  * We need some participants with domain expertise, e.g., a strong link to the SubWG Infrastructure Service Characteristics
  * As we technically implement the concepts produced by multiple other WGs (including Data Exchange, Service Composition, etc.), we need links to them
  * Technically, we need a core group of people who know how to implement schema definitions in LinkML, how to write test, and how to operate our CI infrastructure

# Critical Success Factors (Please mention all critical success factors)

Constraints (Please list any conditions that may limit the Groups options with respect to personnel or timeline):

* Human resources, i.e., people having expert knowledge and capacity
* For the technical delivery perspective (e.g., Registry), we need a strong link and regular sync with the CTO Office.

# Contribution to the Requirements to Release process and Gaia-X release strategy  

For any release, the machine-executable credential schema (especially the SHACL shapes) is strictly required to end up in the Gaia-X Registry.

R2R process: Active participation in the sprints (at least 3 sprint streams planned), assigning subject matter experts with an active role in requirements definition and contribution to the cross-functional sprint teams by delivering increments (agile approach) following agreed release plans and roadmap. 

0. No.	Sprint stream 	Comments 	Contact person
1. Data Exchange Policies (Base Conformance, Configurable Template, 
Trust Anchor List (incl. update process)		
2. Conformity Accessement Bodies Programme – final version 		
3. Improvements needed for v24.04
