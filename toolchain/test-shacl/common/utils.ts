import { expect, expectTypeOf } from 'vitest'
import { ShaclService } from '../src/ShaclService'

import { default as ValidationReport, default as ValidationResult } from 'rdf-validate-shacl/src/validation-report'

import DatasetExt from 'rdf-ext/lib/Dataset'

export const GX_PREFIX = 'https://w3id.org/gaia-x/development#'
export const SCHEMA_PREFIX = 'https://schema.org/'
export const EX_PREFIX = 'http://example.com/'
export const XSD_PREFIX = 'http://www.w3.org/2001/XMLSchema#'
export const VCARD_PREFIX = 'http://www.w3.org/2006/vcard/ns#'
export const QUDT_PREFIX = 'http://qudt.org/schema/qudt/'
export const DCAT_PREFIX = 'http://www.w3.org/ns/dcat#'
export const DCT_PREFIX = 'http://purl.org/dc/terms/'
export const ODRL_PREFIX = 'http://www.w3.org/ns/odrl/2/'

export const CLASS_CONSTRAINT = 'http://www.w3.org/ns/shacl#ClassConstraintComponent'
export const NODE_KIND_CONSTRAINT = 'http://www.w3.org/ns/shacl#NodeKindConstraintComponent'
export const MIN_COUNT_CONSTRAINT = 'http://www.w3.org/ns/shacl#MinCountConstraintComponent'
export const MAX_COUNT_CONSTRAINT = 'http://www.w3.org/ns/shacl#MaxCountConstraintComponent'
export const IN_CONSTRAINT = 'http://www.w3.org/ns/shacl#InConstraintComponent'
export const OR_CONSTRAINT = 'http://www.w3.org/ns/shacl#OrConstraintComponent'
export const DATA_TYPE_CONSTRAINT = 'http://www.w3.org/ns/shacl#DatatypeConstraintComponent'
export const DATA_PATTERN_CONSTRAINT = 'http://www.w3.org/ns/shacl#PatternConstraintComponent'
export const PATTERN_CONSTRAINT = 'http://www.w3.org/ns/shacl#PatternConstraintComponent'
export const CLASS_CONSTRAINT_MESSAGE = 'http://www.w3.org/ns/shacl#IRI'
export const MIN_INCLUSIVE_CONSTRAINT = 'http://www.w3.org/ns/shacl#MinInclusiveConstraintComponent'
export const MAX_INCLUSIVE_CONSTRAINT = 'http://www.w3.org/ns/shacl#MaxInclusiveConstraintComponent'

export const ERR_MESSAGE_NOT_OF_TYPE_STRING = 'Value does not have datatype <' + XSD_PREFIX + 'string>'
export const ERR_MESSAGE_NOT_OF_TYPE_ANY_URI = 'Value does not have datatype <' + XSD_PREFIX + 'anyURI>'
export const ERR_MESSAGE_NOT_OF_TYPE_FLOAT = 'Value does not have datatype <' + XSD_PREFIX + 'float>'
export const ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN = 'Value does not have datatype <' + XSD_PREFIX + 'boolean>'
export const ERR_MESSAGE_NOT_OF_TYPE_INTEGER = 'Value does not have datatype <' + XSD_PREFIX + 'integer>'
export const ERR_MESSAGE_NOT_OF_TYPE_DATE_TIME = 'Value does not have datatype <' + XSD_PREFIX + 'dateTime>'
export const ERR_MESSAGE_NOT_OF_TYPE_DATE = 'Value does not have datatype <' + XSD_PREFIX + 'date>'

// See https://www.w3.org/TR/shacl/#results-validation-result for details
// Displays the result of the validation process that reports the conformance of the test case.
export function displayReport(report: ValidationReport) {
  for (const result of report.results.sort((a, b) => (a.path! < b.path! ? -1 : 1))) {
    console.log('message:', result.message)
    console.log('sh:path:', result.path)
    console.log('sh:focusNode:', result.focusNode)
    console.log('sh:severity:', result.severity)
    console.log('sh:sourceConstraintComponent:', result.sourceConstraintComponent)
    console.log('sh:sourceShape:', result.sourceShape)
    console.log('------------------------------------------------------------')
  }
}

// Checks whether a concrete constraint is present in the result of the validation process, and returns the filtered report
export function filterResults(report: ValidationReport, focusNode: string, path: string, constraint: string): ValidationResult[] {
  let filteredList: ValidationResult[] = []
  if (report != null) {
    for (const result of report.results) {
      let resultFocusNode = result.focusNode
      let resultPath = result.path
      let resultConstraint = result.sourceConstraintComponent

      if (resultFocusNode == null || resultConstraint == null || resultPath == null) {
        console.log('All values are null')
        continue
      }

      if ((focusNode == null || resultFocusNode.value.toString() === focusNode) && (path == null || resultPath.value.toString() == path) && (constraint == null || resultConstraint.value.toString() === constraint)) {
        filteredList = filteredList.concat(result)
      }
    }
  }

  return filteredList
}

// Checks whether a concrete constraint is present in the result of the consistency validation process, and returns the filtered report
export function filterConsistency(report: ValidationReport, focusNode: string, sourceShape: string, constraint: string): ValidationResult[] {
  let filteredList: ValidationResult[] = []
  if (report != null) {
    for (const result of report.results) {
      let resultFocusNode = result.focusNode
      let resultSourceShape = result.sourceShape
      let resultConstraint = result.sourceConstraintComponent

      if (resultFocusNode == null || resultConstraint == null || resultSourceShape == null) {
        console.log('All values are null')
        continue
      }

      if ((focusNode == null || resultFocusNode.value.toString() === focusNode) && (sourceShape == null || resultSourceShape.value.toString() == sourceShape) && (constraint == null || resultConstraint.value.toString() === constraint)) {
        filteredList = filteredList.concat(result)
      }
    }
  }

  return filteredList
}

export async function testValidInstance(data: DatasetExt, shape: DatasetExt, shaclService: ShaclService) {
  expect(data).not.toBeNull()
  expect(data).toBeInstanceOf(DatasetExt)

  const report = await shaclService.validate(data)
  if (!report.conforms) displayReport(report)

  expectTypeOf(report.results).toBeArray()
  expect(report.results).toHaveLength(0)
  expectTypeOf(report.conforms).toBeBoolean()
  expect(report.conforms).toBeTruthy()
}

// Checks whether a REQUIRED attribute has been included in the data graph test case
export function test_min_vio(report: ValidationReport, focusNode: string, resultPath: string) {
  let violations: ValidationResult[] = filterResults(report, focusNode, resultPath, MIN_COUNT_CONSTRAINT)

  expectTypeOf(violations).toBeArray()
  expect(violations, "Number of minimum count violations (sh:focusNode: '" + focusNode + "'; sh:resultPath: '" + resultPath + ')').length.greaterThanOrEqual(1)
  expect(violations[0].message.toString(), "Result message (sh:focusNode: '" + focusNode + "'; sh:resultPath: '" + resultPath + "')").toBe('Less than 1 values')
}

// Checks whether a NON MULTIVALUED attribute has been included more than once in the data graph test case
export function test_max_vio(report: ValidationReport, focusNode: string, resultPath: string) {
  let violations = filterResults(report, focusNode, resultPath, MAX_COUNT_CONSTRAINT)

  expectTypeOf(violations).toBeArray()
  expect(violations, "Number of maximum count violations (sh:focusNode: '" + focusNode + "'; sh:resultPath: '" + resultPath + ')').length.greaterThanOrEqual(1)
  expect(violations[0].message.toString(), "Result message (sh:focusNode: '" + focusNode + "'; sh:resultPath: '" + resultPath + "')").toBe('More than 1 values')
}

// Checks whether the attribute type matches the data graph test case
export function test_data_type(report: ValidationReport, focusNode: string, resultPath: string, constraint: string, error_message: string) {
  let violations = filterResults(report, focusNode, resultPath, constraint)

  expectTypeOf(violations).toBeArray()
  expect(violations, "Number of data type violations (sh:focusNode: '" + focusNode + "'; sh:resultPath: '" + resultPath + "')").length.greaterThanOrEqual(1)
  if (error_message != '') expect(violations[0].message.toString(), "Data type (sh:focusNode: '" + focusNode + "'; sh:resultPath: '" + resultPath + "')").toBe(error_message)
}

export function test_consistency_rule(report: ValidationReport, focusNode: string, sourceShape: string, constraint: string, error_message: string) {
  let violations = filterConsistency(report, focusNode, sourceShape, constraint)

  expectTypeOf(violations).toBeArray()
  expect(violations, "Number of data type violations (sh:focusNode: '" + focusNode + "'; sh:sourceShape: '" + sourceShape + "')").toHaveLength(1)
  if (error_message != '') expect(violations[0].message.toString(), "Data type (sh:focusNode: '" + focusNode + "'; sh:sourceShape: '" + sourceShape + "')").toBe(error_message)
}

// Expected number of errors that the validation process report should contain according to the defined test case
export function checkNumberOfViolations(report: ValidationReport, number_of_errors: number) {
  expectTypeOf(report.conforms).toBeBoolean()
  expect(report.conforms).not.toBeTruthy()

  expectTypeOf(report.results).toBeArray()

  if (report.results.length != number_of_errors) {
    displayReport(report)
  }

  expect(report.results).toHaveLength(number_of_errors)
}
