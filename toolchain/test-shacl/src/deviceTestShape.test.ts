import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Device Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/device-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    //  No mandatory attributes

    test_max_vio(report, EX_PREFIX + 'myDeviceMaxVio', GX_PREFIX + 'vendor')
    test_max_vio(report, EX_PREFIX + 'myDeviceMaxVio', GX_PREFIX + 'generation')
    test_max_vio(report, EX_PREFIX + 'myDeviceMaxVio', GX_PREFIX + 'defaultOversubscriptionRatio')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/device-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_data_type(report, EX_PREFIX + 'myDeviceWrongValues', GX_PREFIX + 'vendor', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myDeviceWrongValues', GX_PREFIX + 'generation', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myDeviceWrongValues', GX_PREFIX + 'defaultOversubscriptionRatio', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myDeviceWrongValues', GX_PREFIX + 'supportedOversubscriptionRatio', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/device-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
