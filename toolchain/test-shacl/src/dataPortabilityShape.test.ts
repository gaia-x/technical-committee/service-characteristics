import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Data Portability Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Data Portability instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-portability-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Data Portability missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-portability-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 8)

    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'resource')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'means')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'documentations')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'legalDocument')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'deletionMethods')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'deletionTimeframe')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'formats')
    test_min_vio(report, EX_PREFIX + 'DataPortabilityMinVio', GX_PREFIX + 'pricing')
  })

  test('Data Portability with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-portability-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 9)

    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'resource', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'means', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'documentations', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'legalDocument', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'deletionMethods', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'deletionTimeframe', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'formats', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'DataPortability', GX_PREFIX + 'pricing', DATA_TYPE_CONSTRAINT, '')
  })
})
