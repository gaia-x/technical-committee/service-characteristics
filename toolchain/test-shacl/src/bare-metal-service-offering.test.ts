import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, NODE_KIND_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Bare Metal Service Offering Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/bare-metal-service-offering-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/bare-metal-service-offering-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    //  Test min count of attributes
    test_min_vio(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'instantiationReq')
    test_min_vio(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'codeArtifact')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/bare-metal-service-offering-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 8)

    test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'instantiationReq', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'instantiationReq', NODE_KIND_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'codeArtifact', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'codeArtifact', NODE_KIND_CONSTRAINT, '')
  })
})
