import ParserJsonLD from '@rdfjs/parser-jsonld'
import ParserN3 from '@rdfjs/parser-n3'
import * as console from 'console'
import * as fs from 'fs'
import rdf from 'rdf-ext'
import DatasetExt from 'rdf-ext/lib/Dataset'
import SHACLValidator from 'rdf-validate-shacl'
import { Readable } from 'stream'
import { DocumentLoader } from './DocumentLoader'

export class ShaclService {
  get validator(): SHACLValidator {
    return this._validator
  }

  set validator(value: SHACLValidator) {
    this._validator = value
  }

  private _validator: SHACLValidator
  private _ontology: DatasetExt

  async validate(data: DatasetExt) {
    return this._validator.validate(this._ontology.merge(data))
  }

  async loadOntologyFromTurtleFile(path: string): Promise<void> {
    try {
      const parser = new ParserN3({ factory: rdf as any })
      const stream = fs.createReadStream(path)
      this._ontology = await rdf.dataset().import(parser.import(stream))
      this._ontology = this._ontology.filter((quad) => quad.predicate.value === 'http://www.w3.org/2000/01/rdf-schema#subClassOf')
    } catch (error) {
      console.error(error)
      throw new Error('Cannot load from provided turtle ontology file.')
    }
  }

  async loadShapesFromTurtleFile(filename: string): Promise<DatasetExt> {
    try {
      const parser = new ParserN3({ factory: rdf as any })
      const stream = fs.createReadStream(filename)
      const shapes = await rdf.dataset().import(parser.import(stream))

      this._validator = new SHACLValidator(shapes, { factory: rdf })

      return shapes
    } catch (error) {
      console.error(error)
      throw new Error('Cannot load from provided turtle shapes file.')
    }
  }

  async loadFromTurtle(raw: string): Promise<DatasetExt> {
    try {
      const parser = new ParserN3({ factory: rdf as any })
      return this.transformToStream(raw, parser)
    } catch (error) {
      console.error(error)
      throw new Error('Cannot load from provided turtle.')
    }
  }

  async loadFromJsonLD(raw: string): Promise<DatasetExt> {
    try {
      const parser = new ParserJsonLD({ factory: rdf })
      return this.transformToStream(raw, parser)
    } catch (error) {
      console.error(error)
      throw new Error('Cannot load from provided JsonLD.')
    }
  }

  async loadFromJsonLDFile(filename: string): Promise<DatasetExt> {
    try {
      const parser = new ParserJsonLD({ factory: rdf })
      const stream = fs.createReadStream(filename)

      const documentLoader: DocumentLoader = new DocumentLoader()

      return await rdf.dataset().import(
        parser.import(stream, {
          documentLoader,
        }),
      )
    } catch (error) {
      console.error(error)
      throw new Error('Cannot load from provided JsonLD.')
    }
  }

  async merge(data1: DatasetExt, data2: DatasetExt) {
    for (const quad of data2) {
      data1.add(quad)
    }
    return data1
  }

  private async transformToStream(raw: string, parser: any): Promise<DatasetExt> {
    const stream = new Readable()
    stream.push(raw)
    stream.push(null)
    return await rdf.dataset().import(parser.import(stream))
  }
}
