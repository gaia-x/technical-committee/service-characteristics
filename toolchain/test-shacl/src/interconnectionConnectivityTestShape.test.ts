import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, MIN_INCLUSIVE_CONSTRAINT, NODE_KIND_CONSTRAINT, test_data_type, test_max_vio } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Interconnection Connectivity Service Offering Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/interconnection-connectivity-service-offering-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_max_vio(report, EX_PREFIX + 'myInterconnectionServiceOfferingMaxVio', GX_PREFIX + 'vlanConfiguration')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/interconnection-connectivity-service-offering-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 27)

    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectivityConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectivityConfiguration', NODE_KIND_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectivityQoS', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectivityQoS', NODE_KIND_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectedNetworkA', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectedNetworkA', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 0')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectedNetworkZ', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectedNetworkZ', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 0')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'prefixSetA', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'prefixSetZ', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'vlanConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'connectionType', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myInterconnectionServiceOfferingWrongVal', GX_PREFIX + 'interfaceType', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })
  /*    
    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/interconnection-connectivity-service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
*/
})