import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Network Connectivity Service Offering Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/network-connectivity-service-offering-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_min_vio(report, EX_PREFIX + 'myNetworkConnectivityConfigurationMinVio', GX_PREFIX + 'connectivityConfiguration')

    test_max_vio(report, EX_PREFIX + 'myNetworkConnectivityConfigurationMaxVio', GX_PREFIX + 'serviceType')
    test_max_vio(report, EX_PREFIX + 'myNetworkConnectivityConfigurationMaxVio', GX_PREFIX + 'publicIpAddressProvisioning')
    test_max_vio(report, EX_PREFIX + 'myNetworkConnectivityConfigurationMaxVio', GX_PREFIX + 'ipVersion')
    test_max_vio(report, EX_PREFIX + 'myNetworkConnectivityConfigurationMaxVio', GX_PREFIX + 'tenantSeparation')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/network-connectivity-service-offering-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 16)

    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'connectivityConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'connectivityQoS', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'serviceType', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'publicIpAddressProvisioning', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'ipVersion', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myNetworkConnectivityConfigurationWrongValues', GX_PREFIX + 'tenantSeparation', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/network-connectivity-service-offering-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})