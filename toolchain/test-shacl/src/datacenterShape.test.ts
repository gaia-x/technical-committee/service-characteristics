import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, OR_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Datacenter Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/datacenter-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    //  datacenter.aggregatedOf indicates no AZ
    test_min_vio(report, EX_PREFIX + 'myDatacenterWrongCard', GX_PREFIX + 'aggregationOfResources')
    test_min_vio(report, EX_PREFIX + 'myDatacenterWrongCard', GX_PREFIX + 'location')
    test_min_vio(report, EX_PREFIX + 'myDatacenterWrongCard', GX_PREFIX + 'maintainedBy')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/datacenter-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 11)

    test_data_type(report, EX_PREFIX + 'myDatacenterWrongValues', GX_PREFIX + 'aggregationOfResources', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDatacenterWrongValues', GX_PREFIX + 'location', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDatacenterWrongValues', GX_PREFIX + 'maintainedBy', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/datacenter-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
