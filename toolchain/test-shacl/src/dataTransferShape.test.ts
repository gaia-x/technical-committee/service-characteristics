import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Data Transfer Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Data Transfer instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-transfer-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Data Transfer missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-transfer-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_min_vio(report, EX_PREFIX + 'DataTransferMinVio', GX_PREFIX + 'reason')
    test_min_vio(report, EX_PREFIX + 'DataTransferMinVio', GX_PREFIX + 'scope')
  })

  test('Data Transfer with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-transfer-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_data_type(report, EX_PREFIX + 'DataTransfer', GX_PREFIX + 'reason', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'DataTransfer', GX_PREFIX + 'scope', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })
})
