import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('VM Image Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/pxe-image-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 1)

    //  Test max count of attributes
    test_max_vio(report, EX_PREFIX + 'myPXEImageMaxVio', GX_PREFIX + 'pxeImageDiskFormat')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/pxe-image-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_data_type(report, EX_PREFIX + 'myPXEImage', GX_PREFIX + 'pxeImageDiskFormat', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/pxe-image-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
