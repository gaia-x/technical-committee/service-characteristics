import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, DCT_PREFIX, ERR_MESSAGE_NOT_OF_TYPE_ANY_URI, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('StandardConformity Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/standard-conformity-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_min_vio(report, EX_PREFIX + 'myStandardConformityMinVio', DCT_PREFIX + 'title')
    test_min_vio(report, EX_PREFIX + 'myStandardConformityMinVio', GX_PREFIX + 'standardReference')
    test_max_vio(report, EX_PREFIX + 'myStandardConformityMaxVio', DCT_PREFIX + 'title')
    test_max_vio(report, EX_PREFIX + 'myStandardConformityMaxVio', GX_PREFIX + 'standardReference')
    test_max_vio(report, EX_PREFIX + 'myStandardConformityMaxVio', GX_PREFIX + 'publisher')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/standard-conformity-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'myStandardConformity', DCT_PREFIX + 'title', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myStandardConformity', GX_PREFIX + 'standardReference', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_ANY_URI)
    test_data_type(report, EX_PREFIX + 'myStandardConformity', GX_PREFIX + 'publisher', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/standard-conformity-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
