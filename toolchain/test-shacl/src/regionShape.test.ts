import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, OR_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Region Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/region-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    //  datacenter.aggregatedOf indicates no AZ
    test_min_vio(report, EX_PREFIX + 'mRegionWrongCard', GX_PREFIX + 'aggregationOfResources')
  })
  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/region-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    //  datacenter.aggregationOf
    test_data_type(report, EX_PREFIX + 'myRegionWrongValues', GX_PREFIX + 'aggregationOfResources', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myRegionWrongValues', GX_PREFIX + 'address', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/region-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
