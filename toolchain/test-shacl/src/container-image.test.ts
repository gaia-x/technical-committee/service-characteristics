import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Container Image Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/container-image-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 18)

    //  Test max count of attributes in ContainerImage
    test_min_vio(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'copyrightOwnedBy')
    test_min_vio(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'license')
    test_min_vio(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'resourcePolicy')
    test_min_vio(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'containerFormat')
    test_max_vio(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'baseContainerImage')
    // Test missing attribute in ContainerBaseImage
    test_min_vio(report, EX_PREFIX + 'myAppWrongTags', GX_PREFIX + 'containerImageLink')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/container-image-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'containerFormat', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myContainerImage', GX_PREFIX + 'baseContainerImage', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/container-image-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
