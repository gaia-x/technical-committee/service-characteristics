import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Water Usage Effectiveness Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Water Usage Effectiveness instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/water-usage-effectiveness-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Water Usage Effectiveness missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/water-usage-effectiveness-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_min_vio(report, EX_PREFIX + 'WaterUsageEffectivenessMinVio', GX_PREFIX + 'waterUsageEffectiveness')
  })

  test('Water Usage Effectiveness with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/water-usage-effectiveness-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'WaterUsageEffectiveness', GX_PREFIX + 'waterUsageEffectiveness', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
    test_data_type(report, EX_PREFIX + 'WaterUsageEffectiveness', GX_PREFIX + 'certifications', CLASS_CONSTRAINT, '')
  })
})
