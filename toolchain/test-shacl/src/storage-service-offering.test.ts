import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, ERR_MESSAGE_NOT_OF_TYPE_INTEGER, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Storage Service Offering Shapes', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })
  test('Valid instances', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/storage-service-offering-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('Storage Service Offering Shapes with wrong cardinalities', () => {
  test('Wrong cardinalities in instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/storage-service-offering-wrong-card.json')
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 16)

    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'minimumSize')
    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'maximumSize')
    test_min_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConfiguration')
    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'lifetimeManagement')
    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'versioning')
    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConsistency')
    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'dataViews')
    test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'multipleViews')

    test_min_vio(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'storageConfiguration')
    test_max_vio(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'accessSemantics')

    test_min_vio(report, EX_PREFIX + 'myBlockStorageService', GX_PREFIX + 'storageConfiguration')

    test_min_vio(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'storageConfiguration')
    test_max_vio(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'maximumObjectSize')
  })
})

describe('Storage Service Offering Shapes with wrong values', () => {
  test('Wrong values in instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/storage-service-offering-wrong-values.json')
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 29)

    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'minimumSize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'maximumSize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'lifetimeManagement', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'versioning', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConsistency', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'dataViews', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'multipleViews', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)

    test_data_type(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'accessSemantics', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'accessAttributes', IN_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myBlockStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'maximumObjectSize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'accessAttributes', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'objectAPICompatibility', IN_CONSTRAINT, '')
  })
})
