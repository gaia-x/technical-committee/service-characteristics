import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Contact Information Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/contact-information-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'myContactInformation', GX_PREFIX + 'email', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myContactInformation', GX_PREFIX + 'phoneNumber', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myContactInformation', GX_PREFIX + 'url', DATA_TYPE_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/contact-information-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
