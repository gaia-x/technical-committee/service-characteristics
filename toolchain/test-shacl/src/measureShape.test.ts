import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, SCHEMA_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Measure Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Measure instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/measure-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Measure missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/measure-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_min_vio(report, EX_PREFIX + 'MeasureMinVio', SCHEMA_PREFIX + 'description')
    test_min_vio(report, EX_PREFIX + 'MeasureMinVio', GX_PREFIX + 'legalDocuments')
  })

  test('Measure with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/measure-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'Measure', SCHEMA_PREFIX + 'description', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'Measure', GX_PREFIX + 'legalDocuments', CLASS_CONSTRAINT, '')
  })
})
