import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Legitimate Interest Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Legitimate Interest instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/legitimate-interest-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Legitimate Interest missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/legitimate-interest-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_min_vio(report, EX_PREFIX + 'LegitimateInterestMinVio', GX_PREFIX + 'legalBasis')
    test_min_vio(report, EX_PREFIX + 'LegitimateInterestMinVio', GX_PREFIX + 'dataProtectionContact')
  })

  test('Legitimate Interest with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/legitimate-interest-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_data_type(report, EX_PREFIX + 'LegitimateInterest', GX_PREFIX + 'legalBasis', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'LegitimateInterest', GX_PREFIX + 'dataProtectionContact', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })
})
