import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'
import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, OR_CONSTRAINT, test_consistency_rule, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Data Resource Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-resource-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 10)

    test_min_vio(report, EX_PREFIX + 'myDataResourceMinVio', GX_PREFIX + 'containsPII')
    test_min_vio(report, EX_PREFIX + 'myDataResourceMinVio', GX_PREFIX + 'producedBy')
    test_min_vio(report, EX_PREFIX + 'myDataResourceMinVio', GX_PREFIX + 'exposedThrough')
    test_min_vio(report, EX_PREFIX + 'myDataResourceMinVio', GX_PREFIX + 'copyrightOwnedBy')
    test_min_vio(report, EX_PREFIX + 'myDataResourceMinVio', GX_PREFIX + 'license')
    test_min_vio(report, EX_PREFIX + 'myDataResourceMinVio', GX_PREFIX + 'resourcePolicy')
    test_max_vio(report, EX_PREFIX + 'myDataResourceMaxVio', GX_PREFIX + 'producedBy')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-resource-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 11)

    test_data_type(report, EX_PREFIX + 'myDataResourceWrongValues', GX_PREFIX + 'containsPII', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myDataResourceWrongValues', GX_PREFIX + 'license', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataResourceWrongValues', GX_PREFIX + 'resourcePolicy', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myDataResourceWrongValues', GX_PREFIX + 'producedBy', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataResourceWrongValues', GX_PREFIX + 'copyrightOwnedBy', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataResourceWrongValues', GX_PREFIX + 'exposedThrough', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-resource-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('Consent Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/consent-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_min_vio(report, EX_PREFIX + 'myConsentMinVio', GX_PREFIX + 'legalBasis')
    test_consistency_rule(report, EX_PREFIX + 'myConsentMinVio', GX_PREFIX + 'ConsentShape', OR_CONSTRAINT, '')
    test_max_vio(report, EX_PREFIX + 'myConsentMaxVio', GX_PREFIX + 'legalBasis')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/consent-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    test_data_type(report, EX_PREFIX + 'myConsentWrongValues', GX_PREFIX + 'legalBasis', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myConsentWrongValues', GX_PREFIX + 'dataProtectionContactPoint', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myConsentWrongValues', GX_PREFIX + 'purpose', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myConsentWrongValues', GX_PREFIX + 'consentWithdrawalContactPoint', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/consent-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/consent-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Consistency attribute invalid rules', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/consent-consistency-invalid-rules.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_consistency_rule(report, EX_PREFIX + 'myConsentInvalid', GX_PREFIX + 'ConsentShape', OR_CONSTRAINT, '')
    test_consistency_rule(report, EX_PREFIX + 'myConsentInvalid2', GX_PREFIX + 'ConsentShape', OR_CONSTRAINT, '')
  })

  test('Consistency attribute valid rules', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/consent-consistency-valid-rules.json')
    await testValidInstance(data, shape, shaclService)
  })
})
