import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, OR_CONSTRAINT, test_data_type, testValidInstance, VCARD_PREFIX } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('AvailabilityZone Shape', () => {
  test('Check loaded shape', () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/availability-zone-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    //  availabilityZone.aggregationOf
    test_data_type(report, EX_PREFIX + 'myAvailabilityZoneWrongValues', GX_PREFIX + 'aggregationOfResources', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myAddress', GX_PREFIX + 'countryCode', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myAddress', VCARD_PREFIX + 'region', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/availability-zone-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
