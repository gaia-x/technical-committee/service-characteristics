import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, SCHEMA_PREFIX, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Gaia-X Entity Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/gaia-x-entity-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_max_vio(report, EX_PREFIX + 'gaiaXEntityMaxVio', SCHEMA_PREFIX + 'name')
    test_max_vio(report, EX_PREFIX + 'gaiaXEntityMaxVio', SCHEMA_PREFIX + 'description')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/gaia-x-entity-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_data_type(report, EX_PREFIX + 'myGaiaXEntity', SCHEMA_PREFIX + 'name', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myGaiaXEntity', SCHEMA_PREFIX + 'description', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/gaia-x-entity-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
