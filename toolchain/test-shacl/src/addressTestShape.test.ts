import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, MAX_INCLUSIVE_CONSTRAINT, MIN_INCLUSIVE_CONSTRAINT, OR_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance, VCARD_PREFIX } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Address Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/address-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_min_vio(report, EX_PREFIX + 'myAddressMinVio', GX_PREFIX + 'countryCode')
    test_max_vio(report, EX_PREFIX + 'myAddressMaxVio', GX_PREFIX + 'countryCode')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/address-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_data_type(report, EX_PREFIX + 'myAddress', GX_PREFIX + 'countryCode', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myAddress', VCARD_PREFIX + 'region', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/address-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('GPS Location Shape', () => {
  test('GPS wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/gps-location-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_min_vio(report, EX_PREFIX + 'myGPSLocation', GX_PREFIX + 'longitude')
    test_min_vio(report, EX_PREFIX + 'myGPSLocation', GX_PREFIX + 'latitude')
  })

  test('GPS wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/gps-location-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'myGPSLocationTypeVio', GX_PREFIX + 'altitude', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myLatitudeRangeMinVio', GX_PREFIX + 'minutes', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 0')
    test_data_type(report, EX_PREFIX + 'myLatitudeRangeMaxVio', GX_PREFIX + 'seconds', MAX_INCLUSIVE_CONSTRAINT, 'Value is not less than or equal to 60')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/gps-location-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
