import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Disk Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/disk-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    test_min_vio(report, EX_PREFIX + 'myDiskMinVio', GX_PREFIX + 'diskSize')
    test_max_vio(report, EX_PREFIX + 'myDiskMaxVio', GX_PREFIX + 'diskSize')
    test_max_vio(report, EX_PREFIX + 'myDiskMaxVio', GX_PREFIX + 'diskType')
    test_max_vio(report, EX_PREFIX + 'myDiskMaxVio', GX_PREFIX + 'diskBusType')
  })
  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/disk-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    test_data_type(report, EX_PREFIX + 'myDiskWrongValues', GX_PREFIX + 'diskSize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDiskWrongValues', GX_PREFIX + 'diskType', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDiskWrongValues', GX_PREFIX + 'diskBusType', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/disk-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
