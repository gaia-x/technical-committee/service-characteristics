import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, ERR_MESSAGE_NOT_OF_TYPE_INTEGER, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, MIN_INCLUSIVE_CONSTRAINT, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('CPU Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cpu-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 8)

    //  Test max count of attributes
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'cpuArchitecture')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'smtEnabled')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'numberOfCores')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'numberOfThreads')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'baseFrequency')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'boostFrequency')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'lastLevelCacheSize')
    test_max_vio(report, EX_PREFIX + 'myCPUMaxVio', GX_PREFIX + 'thermalDesignPower')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cpu-wrong-values.json')
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 17)

    //  Wrong data type
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'cpuArchitecture', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'cpuFlag', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'smtEnabled', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'numberOfCores', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'numberOfThreads', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'baseFrequency', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'boostFrequency', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'lastLevelCacheSize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCPUDataTypeVio', GX_PREFIX + 'thermalDesignPower', CLASS_CONSTRAINT, '')

    //  Value not in range
    test_data_type(report, EX_PREFIX + 'myCPUValueRestrictionVio', GX_PREFIX + 'numberOfCores', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
    test_data_type(report, EX_PREFIX + 'myCPUValueRestrictionVio', GX_PREFIX + 'numberOfThreads', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cpu-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
