import { describe, test } from 'vitest'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, NODE_KIND_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Compliance', () => {
  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/compliance-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/compliance-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_min_vio(report, EX_PREFIX + 'labelCredentialMin', GX_PREFIX + 'labelLevel')
    test_min_vio(report, EX_PREFIX + 'labelCredentialMin', GX_PREFIX + 'engineVersion')
    test_min_vio(report, EX_PREFIX + 'labelCredentialMin', GX_PREFIX + 'rulesVersion')
    test_min_vio(report, EX_PREFIX + 'labelCredentialMin', GX_PREFIX + 'validatedCriteria')
    test_min_vio(report, EX_PREFIX + 'labelCredentialMin', GX_PREFIX + 'compliantCredentials')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/compliance-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_data_type(report, EX_PREFIX + 'labelCredentialValues', GX_PREFIX + 'labelLevel', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'labelCredentialValues', GX_PREFIX + 'engineVersion', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'labelCredentialValues', GX_PREFIX + 'rulesVersion', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'labelCredentialValues', GX_PREFIX + 'validatedCriteria', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'labelCredentialValues', GX_PREFIX + 'compliantCredentials', NODE_KIND_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'labelCredentialValues', GX_PREFIX + 'compliantCredentials', CLASS_CONSTRAINT, '')
  })
})
