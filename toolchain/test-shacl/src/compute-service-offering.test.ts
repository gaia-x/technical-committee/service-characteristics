import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Compute Service Offering Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/compute-service-offering-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 1)

    test_max_vio(report, EX_PREFIX + 'myComputeServiceOfferingMaxVio', GX_PREFIX + 'tenantSeparation')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/compute-service-offering-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    test_data_type(report, EX_PREFIX + 'myComputeServiceOffering', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myComputeServiceOffering', GX_PREFIX + 'tenantSeparation', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/compute-service-offering-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
