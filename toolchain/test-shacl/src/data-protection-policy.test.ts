import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, ERR_MESSAGE_NOT_OF_TYPE_INTEGER, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Data Protection Policy Shapes', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid instances', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-protection-policy-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('Data Protection Policy Shapes with wrong cardinalities', () => {
  test('Wrong cardinalities in instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-protection-policy-wrong-card.json')
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 21)

    test_max_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionFrequency')
    test_min_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionRetention')
    test_max_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionMethod')
    test_min_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'backupLocation')

    test_max_vio(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionFrequency')
    test_min_vio(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionRetention')
    test_max_vio(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionMethod')

    test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionFrequency')
    test_min_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionRetention')
    test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionMethod')
    test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'synchronousReplication')
    test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'consistencyType')
  })
})

describe('Data Protection Policy Shapes with wrong values', () => {
  test('Wrong values in instances', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-protection-policy-wrong-values.json')
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 34)

    test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionFrequency', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionRetention', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionMethod', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'backupLocation', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'backupReplication', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionFrequency', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionRetention', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionMethod', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'snapshotReplication', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionFrequency', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionRetention', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionMethod', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'synchronousReplication', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'consistencyType', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'replicaNumber', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'geoReplication', IN_CONSTRAINT, '')
  })
})
