import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Storage Configuration Shapes', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid instances', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/storage-configuration-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('Storage Configuration   Shapes with wrong cardinalities', () => {
  test('Wrong cardinalities in instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/storage-configuration-wrong-card.json')
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 5)

    test_min_vio(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageEncryption')
    test_min_vio(report, EX_PREFIX + 'myFileStorageConfig', GX_PREFIX + 'storageEncryption')
    test_min_vio(report, EX_PREFIX + 'myBlockStorageConfig', GX_PREFIX + 'storageEncryption')
  })
})

describe('Storage Configuration  Shapes with wrong values', () => {
  test('Wrong values in instances', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/storage-configuration-wrong-values.json')
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 23)

    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageCompression', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageDeduplication', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageEncryption', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageRedundancyMechanism', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageProtection', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageQoS', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'blockSize', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myStorageConfig', GX_PREFIX + 'storageEncryption', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myFileStorageConfig', GX_PREFIX + 'fileSystemType', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myFileStorageConfig', GX_PREFIX + 'highLevelAccessProtocol', IN_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myBlockStorageConfig', GX_PREFIX + 'storageEncryption', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBlockStorageConfig', GX_PREFIX + 'blockStorageTechnology', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myBlockStorageConfig', GX_PREFIX + 'lowLevelBlockAccessProtocol', IN_CONSTRAINT, '')
  })
})