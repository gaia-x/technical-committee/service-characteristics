import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, ERR_MESSAGE_NOT_OF_TYPE_FLOAT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, MAX_INCLUSIVE_CONSTRAINT, MIN_INCLUSIVE_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Server Flavor Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/server-flavor-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 12)

    test_min_vio(report, EX_PREFIX + 'myServerFlavorMinVio', GX_PREFIX + 'cpu')
    test_min_vio(report, EX_PREFIX + 'myServerFlavorMinVio', GX_PREFIX + 'memory')
    test_min_vio(report, EX_PREFIX + 'myServerFlavorMinVio', GX_PREFIX + 'bootVolume')

    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'cpu')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'memory')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'gpu')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'network')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'bootVolume')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'confidentialComputing')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'hypervisor')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'hardwareAssistedVirtualization')
    test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'hwRngTypeOfFlavor')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/server-flavor-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 17)

    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'cpu', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'memory', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'bootVolume', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'gpu', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'network', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'additionalVolume', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'confidentialComputing', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'hypervisor', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'hardwareAssistedVirtualization', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'hwRngTypeOfFlavor', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/server-flavor-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('CPU Capabilites Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cpu-capabilities-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_min_vio(report, EX_PREFIX + 'myCpuCapabilitiesMinVio', GX_PREFIX + 'pCPU')
    test_min_vio(report, EX_PREFIX + 'myCpuCapabilitiesMinVio', GX_PREFIX + 'vCPUs')

    test_max_vio(report, EX_PREFIX + 'myCpuCapabilitiesMaxVio', GX_PREFIX + 'pCPU')
    test_max_vio(report, EX_PREFIX + 'myCpuCapabilitiesMaxVio', GX_PREFIX + 'vCPUs')
    test_max_vio(report, EX_PREFIX + 'myCpuCapabilitiesMaxVio', GX_PREFIX + 'overProvisioningRatio')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cpu-capabilities-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_data_type(report, EX_PREFIX + 'myCpuCapabilities', GX_PREFIX + 'pCPU', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCpuCapabilities', GX_PREFIX + 'vCPUs', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myCpuCapabilities', GX_PREFIX + 'vCPUs', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
    test_data_type(report, EX_PREFIX + 'myCpuCapabilities', GX_PREFIX + 'overProvisioningRatio', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
    test_data_type(report, EX_PREFIX + 'myCpuCapabilities', GX_PREFIX + 'overProvisioningRatio', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cpu-capabilities-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('Memory Capabilites Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/memory-capabilities-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_min_vio(report, EX_PREFIX + 'myMemoryCapabilitiesMinVio', GX_PREFIX + 'memory')

    test_max_vio(report, EX_PREFIX + 'myMemoryCapabilitiesMaxVio', GX_PREFIX + 'memory')
    test_max_vio(report, EX_PREFIX + 'myMemoryCapabilitiesMaxVio', GX_PREFIX + 'overProvisioningRatio')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/memory-capabilities-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_data_type(report, EX_PREFIX + 'myMemoryCapabilities1', GX_PREFIX + 'memory', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myMemoryCapabilities1', GX_PREFIX + 'overProvisioningRatio', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
    test_data_type(report, EX_PREFIX + 'myMemoryCapabilities1', GX_PREFIX + 'overProvisioningRatio', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
    test_data_type(report, EX_PREFIX + 'myMemoryCapabilities1', GX_PREFIX + 'overProvisioningRatio', MAX_INCLUSIVE_CONSTRAINT, 'Value is not less than or equal to 3')

    test_data_type(report, EX_PREFIX + 'myMemoryCapabilities2', GX_PREFIX + 'overProvisioningRatio', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
    test_data_type(report, EX_PREFIX + 'myMemoryCapabilities3', GX_PREFIX + 'overProvisioningRatio', MAX_INCLUSIVE_CONSTRAINT, 'Value is not less than or equal to 3')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/memory-capabilities-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
