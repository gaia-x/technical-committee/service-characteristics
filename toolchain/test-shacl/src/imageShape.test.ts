import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, ERR_MESSAGE_NOT_OF_TYPE_DATE, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Image Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/image-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 14)

    //  Test max count of attributes
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'vPMU')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'cpuReq')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'maintenance')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'fileSize')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'multiQueues')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'secureBoot')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'updateStrategy')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'encryption')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'licenseIncluded')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'ramReq')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'operatingSystem')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'rootDiskReq')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'gpuReq')
    test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'videoRamSize')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/image-wrong-values.json')
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 23)

    //  Wrong data type
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'vPMU', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'cpuReq', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'maintenance', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'fileSize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'multiQueues', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'secureBoot', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'updateStrategy', IN_CONSTRAINT, 'Value is not one of the allowed values: maximum_security, minimal_downtime, do_not_update')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'encryption', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'licenseIncluded', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'ramReq', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'operatingSystem', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'rootDiskReq', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'gpuReq', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'videoRamSize', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/image-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})

describe('Maintenance Subscription Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/maintenance-substription-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    //  Test max count of attributes
    test_max_vio(report, EX_PREFIX + 'myMaintenanceSubscriptionMaxVio', GX_PREFIX + 'subscriptionIncluded')
    test_max_vio(report, EX_PREFIX + 'myMaintenanceSubscriptionMaxVio', GX_PREFIX + 'subscriptionRequired')
    test_max_vio(report, EX_PREFIX + 'myMaintenanceSubscriptionMaxVio', GX_PREFIX + 'maintainedUntil')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/maintenance-substription-wrong-values.json')
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 3)

    //  Wrong data type
    test_data_type(report, EX_PREFIX + 'myMaintenanceSubscription', GX_PREFIX + 'subscriptionIncluded', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myMaintenanceSubscription', GX_PREFIX + 'subscriptionRequired', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myMaintenanceSubscription', GX_PREFIX + 'maintainedUntil', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_DATE)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/maintenance-substription-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
