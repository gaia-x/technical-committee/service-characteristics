import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, OR_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('SubContractor Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid SubContractor instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/sub-contractor-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('SubContractor missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/sub-contractor-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    test_min_vio(report, EX_PREFIX + 'SubContractorMinVio', GX_PREFIX + 'applicableJurisdiction')
    test_min_vio(report, EX_PREFIX + 'SubContractorMinVio', GX_PREFIX + 'legalName')
    test_min_vio(report, EX_PREFIX + 'SubContractorMinVio', GX_PREFIX + 'communicationMethods')
    test_min_vio(report, EX_PREFIX + 'SubContractorMinVio', GX_PREFIX + 'informationDocuments')
  })

  test('SubContractor with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/sub-contractor-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    //checkNumberOfViolations(report, 4)

    test_data_type(report, EX_PREFIX + 'SubContractor', GX_PREFIX + 'applicableJurisdiction', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'SubContractor', GX_PREFIX + 'legalName', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'SubContractor', GX_PREFIX + 'communicationMethods', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'SubContractor', GX_PREFIX + 'informationDocuments', OR_CONSTRAINT, '')
  })
})
