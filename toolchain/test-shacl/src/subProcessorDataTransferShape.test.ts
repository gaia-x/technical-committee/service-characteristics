import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('SubProcessor Data Transfer Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid SubProcessor Data Transfer instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/sub-processor-data-transfer-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('SubProcessor Data Transfer missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/sub-processor-data-transfer-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_min_vio(report, EX_PREFIX + 'SubProcessorDataTransferMinVio', GX_PREFIX + 'reason')
    test_min_vio(report, EX_PREFIX + 'SubProcessorDataTransferMinVio', GX_PREFIX + 'scope')
    test_min_vio(report, EX_PREFIX + 'SubProcessorDataTransferMinVio', GX_PREFIX + 'subProcessorManagement')
  })

  test('SubProcessor Data Transfer with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/sub-processor-data-transfer-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 8)

    test_data_type(report, EX_PREFIX + 'SubProcessorDataTransfer', GX_PREFIX + 'reason', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'SubProcessorDataTransfer', GX_PREFIX + 'scope', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'SubProcessorDataTransfer', GX_PREFIX + 'subProcessor', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'SubProcessorDataTransfer', GX_PREFIX + 'subProcessorManagement', CLASS_CONSTRAINT, '')
  })
})
