import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_DATE, ERR_MESSAGE_NOT_OF_TYPE_FLOAT, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Energy Mix Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Energy Mix instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/energy-mix-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Energy Mix missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/energy-mix-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_min_vio(report, EX_PREFIX + 'EnergyMixMinVio', GX_PREFIX + 'attainmentDate')
    test_min_vio(report, EX_PREFIX + 'EnergyMixMinVio', GX_PREFIX + 'renewableEnergy')
    test_min_vio(report, EX_PREFIX + 'EnergyMixMinVio', GX_PREFIX + 'hourlyCarbonFreeEnergy')
  })

  test('Energy Mix with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/energy-mix-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 3)

    test_data_type(report, EX_PREFIX + 'EnergyMix', GX_PREFIX + 'attainmentDate', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_DATE)
    test_data_type(report, EX_PREFIX + 'EnergyMix', GX_PREFIX + 'renewableEnergy', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
    test_data_type(report, EX_PREFIX + 'EnergyMix', GX_PREFIX + 'hourlyCarbonFreeEnergy', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
  })
})
