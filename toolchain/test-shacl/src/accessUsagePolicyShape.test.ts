import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Access Usage Policy Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/access-usage-policy-missing-attribute.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_min_vio(report, EX_PREFIX + 'accessUsagePolicy2', GX_PREFIX + 'policyLanguage')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/access-usage-policy-wrong-values.json')
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 1)

    test_data_type(report, EX_PREFIX + 'accessUsagePolicy3', GX_PREFIX + 'policyLanguage', IN_CONSTRAINT, 'Value is not one of the allowed values: ODRL, XACML, Rego ... (and 2 more)')
  })

  test('Valid ODRL instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/access-usage-policy-odrl-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Valid Rego instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/access-usage-policy-rego-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
