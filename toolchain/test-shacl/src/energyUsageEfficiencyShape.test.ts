import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Energy Usage Efficiency Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Energy Usage Efficiency instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/energy-usage-efficiency-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Energy Usage Efficiency missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/energy-usage-efficiency-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_min_vio(report, EX_PREFIX + 'EnergyUsageEfficiencyMinVio', GX_PREFIX + 'powerUsageEffectiveness')
  })

  test('Energy Usage Efficiency with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/energy-usage-efficiency-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_data_type(report, EX_PREFIX + 'EnergyUsageEfficiency', GX_PREFIX + 'powerUsageEffectiveness', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
  })
})
