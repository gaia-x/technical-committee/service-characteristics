import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Link Connectivity Service Offering Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/link-connectivity-service-offering-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_min_vio(report, EX_PREFIX + 'myLinkConnectivityConfigurationMinVio', GX_PREFIX + 'connectivityConfiguration')
    test_min_vio(report, EX_PREFIX + 'myLinkConnectivityConfigurationMinVio', GX_PREFIX + 'protocolType')

    test_max_vio(report, EX_PREFIX + 'myLinkConnectivityConfigurationMaxVio', GX_PREFIX + 'protocolType')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/link-connectivity-service-offering-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 15)

    test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'connectivityConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'connectivityQoS', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'protocolType', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'vlanConfiguration', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/link-connectivity-service-offering-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})