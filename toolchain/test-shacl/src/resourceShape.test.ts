import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, OR_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Resource Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/resource-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_min_vio(report, EX_PREFIX + 'myVirtualResourceMinVio', GX_PREFIX + 'copyrightOwnedBy')
    test_min_vio(report, EX_PREFIX + 'myVirtualResourceMinVio', GX_PREFIX + 'license')
    test_min_vio(report, EX_PREFIX + 'myVirtualResourceMinVio', GX_PREFIX + 'resourcePolicy')

    test_min_vio(report, EX_PREFIX + 'myPhysicalResourceMinVio', GX_PREFIX + 'maintainedBy')
    test_min_vio(report, EX_PREFIX + 'myPhysicalResourceMinVio', GX_PREFIX + 'location')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/resource-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 18)

    test_data_type(report, EX_PREFIX + 'myResource', GX_PREFIX + 'aggregationOfResources', OR_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myVirtualResource', GX_PREFIX + 'copyrightOwnedBy', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myVirtualResource', GX_PREFIX + 'license', OR_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myVirtualResource', GX_PREFIX + 'resourcePolicy', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)

    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'maintainedBy', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'manufacturedBy', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'ownedBy', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'location', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'energyUsageEfficiency', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'waterUsageEffectiveness', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalResource', GX_PREFIX + 'energyMix', CLASS_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/resource-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
