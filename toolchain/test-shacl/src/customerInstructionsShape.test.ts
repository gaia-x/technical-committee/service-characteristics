import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'

import { checkNumberOfViolations, CLASS_CONSTRAINT, EX_PREFIX, GX_PREFIX, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Customer Instructions Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Customer Instructions instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/customer-instructions-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Customer Instructions missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/customer-instructions-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 2)

    test_min_vio(report, EX_PREFIX + 'CustomerInstructionsMinVio', GX_PREFIX + 'terms')
    test_min_vio(report, EX_PREFIX + 'CustomerInstructionsMinVio', GX_PREFIX + 'means')
  })

  test('Customer Instructions with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/customer-instructions-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 4)

    test_data_type(report, EX_PREFIX + 'CustomerInstructions', GX_PREFIX + 'terms', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'CustomerInstructions', GX_PREFIX + 'means', CLASS_CONSTRAINT, '')
  })
})
