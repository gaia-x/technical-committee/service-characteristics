import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Legal Document Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Legal Document instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/legal-document-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Legal Document missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/legal-document-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 1)

    test_min_vio(report, EX_PREFIX + 'LegalDocumentMinVio', GX_PREFIX + 'url')
  })

  test('Legal Document with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/legal-document-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    test_data_type(report, EX_PREFIX + 'LegalDocument', GX_PREFIX + 'url', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'LegalDocument', GX_PREFIX + 'mimeTypes', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'LegalDocument', GX_PREFIX + 'governingLawCountries', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'LegalDocument', GX_PREFIX + 'involvedParties', CLASS_CONSTRAINT, '')
  })
})
