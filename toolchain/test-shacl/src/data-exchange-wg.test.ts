import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, DCAT_PREFIX, DCT_PREFIX, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT,NODE_KIND_CONSTRAINT, ODRL_PREFIX, SCHEMA_PREFIX, test_data_type, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Data Product Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-product-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 65)

    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'termsAndConditions', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', SCHEMA_PREFIX + 'name', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', DCAT_PREFIX + 'contactPoint', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', DCT_PREFIX + 'license', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', DCT_PREFIX + 'identifier', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', DCT_PREFIX + 'issued', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', ODRL_PREFIX + 'hasPolicy', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'endpoint', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', DCT_PREFIX + 'title', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'servicePolicy', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'servicePolicy', NODE_KIND_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', DCT_PREFIX + 'conformsTo', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'aggregationOf', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'dataAccountExport', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'obsoleteDateTime', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'providedBy', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'serviceOfferingTermsAndConditions', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'keyword', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataProduct#1', GX_PREFIX + 'dataProtectionRegime', IN_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myDataSet#1', SCHEMA_PREFIX + 'name', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', DCT_PREFIX + 'distribution', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', GX_PREFIX + 'exposedThrough', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', DCT_PREFIX + 'identifier', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', GX_PREFIX + 'expirationDateTime', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', DCT_PREFIX + 'license', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', SCHEMA_PREFIX + 'description', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', DCT_PREFIX + 'title', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDataSet#1', DCT_PREFIX + 'issued', DATA_TYPE_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myDistribution#1', SCHEMA_PREFIX + 'name', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', GX_PREFIX + 'hash', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCAT_PREFIX + 'packageFormat', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCT_PREFIX + 'title', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', GX_PREFIX + 'expirationDateTime', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCT_PREFIX + 'license', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCAT_PREFIX + 'byteSize', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCT_PREFIX + 'issued', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCT_PREFIX + 'format', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', GX_PREFIX + 'hashAlgorithm', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', GX_PREFIX + 'location', DATA_TYPE_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myDistribution#1', DCAT_PREFIX + 'compressFormat', DATA_TYPE_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/data-product-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})