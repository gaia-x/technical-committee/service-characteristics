import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Memory Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/memory-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_min_vio(report, EX_PREFIX + 'myMemoryMinVio', GX_PREFIX + 'memorySize')
    test_max_vio(report, EX_PREFIX + 'myMemoryMaxVio', GX_PREFIX + 'memorySize')
    test_max_vio(report, EX_PREFIX + 'myMemoryMaxVio', GX_PREFIX + 'memoryClass')
    test_max_vio(report, EX_PREFIX + 'myMemoryMaxVio', GX_PREFIX + 'memoryRank')
    test_max_vio(report, EX_PREFIX + 'myMemoryMaxVio', GX_PREFIX + 'eccEnabled')
    test_max_vio(report, EX_PREFIX + 'myMemoryMaxVio', GX_PREFIX + 'hardwareEncryption')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/memory-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_data_type(report, EX_PREFIX + 'myMemoryWrongValues', GX_PREFIX + 'memorySize', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myMemoryWrongValues', GX_PREFIX + 'memoryClass', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myMemoryWrongValues', GX_PREFIX + 'memoryRank', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myMemoryWrongValues', GX_PREFIX + 'eccEnabled', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    test_data_type(report, EX_PREFIX + 'myMemoryWrongValues', GX_PREFIX + 'hardwareEncryption', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/memory-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
