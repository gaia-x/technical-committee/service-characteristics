import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Physical Connectivity Service Offering Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/physical-connectivity-service-offering-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_min_vio(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationMinVio', GX_PREFIX + 'connectivityConfiguration')
    test_min_vio(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationMinVio', GX_PREFIX + 'circuitType')
    test_min_vio(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationMinVio', GX_PREFIX + 'interfaceType')

    test_max_vio(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationMaxVio', GX_PREFIX + 'interfaceType')
    test_max_vio(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationMaxVio', GX_PREFIX + 'circuitType')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/physical-connectivity-service-offering-wrong-values.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 14)

    test_data_type(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationWrongValues', GX_PREFIX + 'cryptographicSecurityStandards', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationWrongValues', GX_PREFIX + 'connectivityConfiguration', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationWrongValues', GX_PREFIX + 'connectivityQoS', CLASS_CONSTRAINT, '')

    test_data_type(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationWrongValues', GX_PREFIX + 'circuitType', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'myPhysicalConnectivityConfigurationWrongValues', GX_PREFIX + 'interfaceType', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/physical-connectivity-service-offering-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})