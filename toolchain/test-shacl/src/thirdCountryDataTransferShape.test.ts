import DatasetExt from 'rdf-ext/lib/Dataset'
import { describe, expect, test } from 'vitest'
import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Third Country Data Transfer Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Valid Third Country Data Transfer instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/third-country-data-transfer-valid.json')
    await testValidInstance(data, shape, shaclService)
  })

  test('Third Country Data Transfer missing required fields', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/third-country-data-transfer-wrong-card.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_min_vio(report, EX_PREFIX + 'ThirdCountryDataTransferMinVio', GX_PREFIX + 'reason')
    test_min_vio(report, EX_PREFIX + 'ThirdCountryDataTransferMinVio', GX_PREFIX + 'scope')
    test_min_vio(report, EX_PREFIX + 'ThirdCountryDataTransferMinVio', GX_PREFIX + 'country')
    test_min_vio(report, EX_PREFIX + 'ThirdCountryDataTransferMinVio', GX_PREFIX + 'securingMechanism')
  })

  test('Third Country Data Transfer with incorrect data types', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/third-country-data-transfer-wrong-values.json')

    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 6)

    test_data_type(report, EX_PREFIX + 'ThirdCountryDataTransfer', GX_PREFIX + 'reason', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'ThirdCountryDataTransfer', GX_PREFIX + 'scope', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'ThirdCountryDataTransfer', GX_PREFIX + 'country', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'ThirdCountryDataTransfer', GX_PREFIX + 'securingMechanism', DATA_TYPE_CONSTRAINT, '')
  })
})
