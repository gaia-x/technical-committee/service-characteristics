import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, IN_CONSTRAINT, test_data_type, test_max_vio, test_min_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Cryptopraphy Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cryptography-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 17)

    //  Test Encryption min and max count of attributes
    test_min_vio(report, EX_PREFIX + 'myEncryptionMinVio', GX_PREFIX + 'cipher')
    test_min_vio(report, EX_PREFIX + 'myEncryptionMinVio', GX_PREFIX + 'keyManagement')
    test_max_vio(report, EX_PREFIX + 'myEncryptionMaxVio', GX_PREFIX + 'cipher')
    test_max_vio(report, EX_PREFIX + 'myEncryptionMaxVio', GX_PREFIX + 'keyManagement')

    //  Test CheckSum min and max count of attributes
    test_min_vio(report, EX_PREFIX + 'myCheckSumMinVio', GX_PREFIX + 'checkSumCalculation')
    test_min_vio(report, EX_PREFIX + 'myCheckSumMinVio', GX_PREFIX + 'checkSumValue')
    test_max_vio(report, EX_PREFIX + 'myCheckSumMaxVio', GX_PREFIX + 'checkSumCalculation')
    test_max_vio(report, EX_PREFIX + 'myCheckSumMaxVio', GX_PREFIX + 'checkSumValue')

    // Test Signature min and max count of attributes
    test_min_vio(report, EX_PREFIX + 'mySignatureMinVio', GX_PREFIX + 'hashAlgorithm')
    test_min_vio(report, EX_PREFIX + 'mySignatureMinVio', GX_PREFIX + 'signatureAlgorithm')
    test_min_vio(report, EX_PREFIX + 'mySignatureMinVio', GX_PREFIX + 'signatureValue')
    test_max_vio(report, EX_PREFIX + 'mySignatureMaxVio', GX_PREFIX + 'hashAlgorithm')
    test_max_vio(report, EX_PREFIX + 'mySignatureMaxVio', GX_PREFIX + 'signatureAlgorithm')
    test_max_vio(report, EX_PREFIX + 'mySignatureMaxVio', GX_PREFIX + 'signatureValue')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cryptography-wrong-values.json')
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 7)

    //  Wrong data type
    test_data_type(report, EX_PREFIX + 'myEncryption', GX_PREFIX + 'cipher', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myEncryption', GX_PREFIX + 'keyManagement', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCheckSum', GX_PREFIX + 'checkSumCalculation', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'myCheckSum', GX_PREFIX + 'checkSumValue', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'mySignature', GX_PREFIX + 'signatureAlgorithm', IN_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'mySignature', GX_PREFIX + 'signatureValue', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'mySignature', GX_PREFIX + 'hashAlgorithm', IN_CONSTRAINT, '')
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/cryptography-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
