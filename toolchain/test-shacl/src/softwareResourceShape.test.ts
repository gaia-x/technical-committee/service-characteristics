import { describe, expect, test } from 'vitest'
import DatasetExt from 'rdf-ext/lib/Dataset'

import { checkNumberOfViolations, CLASS_CONSTRAINT, DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_DATE_TIME, ERR_MESSAGE_NOT_OF_TYPE_STRING, EX_PREFIX, GX_PREFIX, test_data_type, test_max_vio, testValidInstance } from '../common/utils'
import { shaclService, shape } from '../vitest.setup'

describe('Software Resource Shape', () => {
  test('Check loaded shape', async () => {
    expect(shape).not.toBeNull()
    expect(shape).toBeInstanceOf(DatasetExt)
  })

  test('Wrong attributes cardinality', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/software-resource-wrong-card.json')

    const report = await shaclService.validate(data)
    checkNumberOfViolations(report, 5)

    //  Test max count of attributes
    test_max_vio(report, EX_PREFIX + 'mySoftwareResourceMaxVio', GX_PREFIX + 'checkSum')
    test_max_vio(report, EX_PREFIX + 'mySoftwareResourceMaxVio', GX_PREFIX + 'signature')
    test_max_vio(report, EX_PREFIX + 'mySoftwareResourceMaxVio', GX_PREFIX + 'version')
    test_max_vio(report, EX_PREFIX + 'mySoftwareResourceMaxVio', GX_PREFIX + 'patchLevel')
    test_max_vio(report, EX_PREFIX + 'mySoftwareResourceMaxVio', GX_PREFIX + 'buildDate')
  })

  test('Wrong attributes values', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/software-resource-wrong-values.json')
    const report = await shaclService.validate(data)

    checkNumberOfViolations(report, 7)

    //  Wrong data type
    test_data_type(report, EX_PREFIX + 'mySoftwareResource', GX_PREFIX + 'checkSum', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'mySoftwareResource', GX_PREFIX + 'signature', CLASS_CONSTRAINT, '')
    test_data_type(report, EX_PREFIX + 'mySoftwareResource', GX_PREFIX + 'version', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'mySoftwareResource', GX_PREFIX + 'patchLevel', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    test_data_type(report, EX_PREFIX + 'mySoftwareResource', GX_PREFIX + 'buildDate', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_DATE_TIME)
  })

  test('Valid instance', async () => {
    let data = await shaclService.loadFromJsonLDFile('data/software-resource-valid.json')
    await testValidInstance(data, shape, shaclService)
  })
})
