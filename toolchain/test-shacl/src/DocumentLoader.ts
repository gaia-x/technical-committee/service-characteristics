import FsDocumentLoader from '@rdfjs/parser-jsonld/FsDocumentLoader'
import { FetchDocumentLoader, IDocumentLoader, IJsonLdContext } from 'jsonld-context-parser'
import path from 'path'

export class DocumentLoader implements IDocumentLoader {
  private readonly fsDocumentLoader: FsDocumentLoader = new FsDocumentLoader({
    'https://w3id.org/gaia-x/development#': path.resolve('../../context.jsonld'),
  })

  private readonly defaultDocumentLoader: FetchDocumentLoader = new FetchDocumentLoader()

  load(url: string): Promise<IJsonLdContext> {
    if (url === 'https://w3id.org/gaia-x/development#') {
      return this.fsDocumentLoader.load(url) as Promise<IJsonLdContext>
    }

    return this.defaultDocumentLoader.load(url)
  }
}
