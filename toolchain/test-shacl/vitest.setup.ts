// @ts-nocheck
import { ShaclService } from './src/ShaclService'

if (!global.shaclService) {
  global.shaclService = new ShaclService()
}

if (!global.shape) {
  global.shape = await global.shaclService.loadShapesFromTurtleFile("../../shapes.shacl.ttl")
  await global.shaclService.loadOntologyFromTurtleFile("../../ontology.owl.ttl")
}

const shaclService = global.shaclService
const shape = global.shape

export {shaclService, shape}